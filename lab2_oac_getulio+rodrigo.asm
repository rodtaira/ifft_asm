
#Programmers:
#Getulio Yassuyuki Matayoshi
#Rodrigo Tsutomu Taira

#Universidade de Brasília
#This programs consists in the Inverse Fast Fourier Transformation writeen in MIPS Assembly 

.data
real:		.space  512
imag:		.space  512
A_real:		.space  512
A_imag:		.space  512
B_real:		.space  512
B_imag:		.space  512

#space0:		.space  32
buffer:		.space  64
	# Constantes usados para algoritmo de CORDIC
thetai:		.double 0.7853981633974480, 0.4636476090008060, 0.2449786631268640, 0.1243549945467610, 
			0.0624188099959574, 0.0312398334302683, 0.0156237286204768, 0.0078123410601011, 
			0.0039062301319670, 0.0019531225164788, 0.0009765621895593, 0.0004882812111949, 
			0.0002441406201494, 0.0001220703118937, 0.0000610351561742, 0.0000305175781155, 
			0.0000152587890613, 0.0000076293945311
space1:		.space  16
nome_arq_saida:	.asciiz "saida.txt"
#space2:		.space  22
nome_arq_ent:	.space  32
#nome_arq_ent:	.asciiz "n.txt"
#space3:		.space  26

texto1:		.asciiz "Nome do arquivo(com extensao): "

erro_arq_abrir:	.asciiz "\nErro ao abrir arquivo de entrada! Verifique se aquivo existe.\n"
erro_arq_criar:	.asciiz "\nErro ao criar arquivo de saida! A operacao vai ser cancelada.\n"
erro_arq_vazio:	.asciiz "\nArquivo de entrada esta vazia! A operacao vai ser cancelada. \n"
erro_n_string:	.asciiz "\nO tamanho superou o limite!  \n"
erro_str_vazio:	.asciiz "\nEntrada nao pode ser vazio!  \n"


.text

.globl	main
main:
	li	$s3, 25		# N / 4
	sll	$s4, $s3, 1	# N / 2
	sll	$s5, $s4, 1	# N
	
	li	$s6, 1		#Constante int 1
	li	$s7, 10		#Constante int 10
	
	
	li	$t0 0xeb1c432d	# 0.00005	0x3f0a36e2eb1c432d
	li	$t1 0x3f0a36e2
	mtc1.d	$t0, $f18
	
	li	$t0, 0x40590000	# 100.0
	mtc1	$t0, $f21
	
	#mtc1	$s5, $f0	# N
	#cvt.s.w	$f0, $f0
	#cvt.d.s	$f20, $f0

	li	$t0 0x54442d28	# PI/2	0x3ff921fb54442d28
	li	$t1 0x3ff921fb
	mtc1.d	$t0, $f22
	
	li	$t0 0x54442d11	# PI 	0x400921fb54442d11
	li	$t1 0x400921fb
	mtc1.d	$t0, $f24
	
	li	$t0 0x54442d1c	# 2PI 	0x401921fb54442d1c
	li	$t1 0x401921fb
	mtc1.d	$t0, $f26
	
	li	$t0, 0x9217271c	# 2PI / 100
	li	$t1, 0x3fb015bf
	mtc1.d	$t0, $f28
	
	#mtc1	$s5, $f0	# 2PI / N
	#cvt.s.w	$f0, $f0
	#cvt.d.s	$f0, $f0
	#div.d	$f28, $f26, $f0
	
	li	$t0 0x48ce9f41	# 1.64676025810509 	0x3ffa592148ce9f41
	li	$t1 0x3ffa5921	
	mtc1.d 	$t0, $f30
	
# #### ABRIR ARQ ENTRADA ####
	la	$a0, texto1
	la	$a1, nome_arq_ent
	jal	ler_string
	li	$v0, 13
	la	$a0, nome_arq_ent
	move	$a1, $zero
	move	$a2, $zero
	syscall
	bne	$v0, -1 ARQ_ENT_ABERTO
	li	$v0, 4
	la	$a0, erro_arq_abrir
	syscall
	j	main
ARQ_ENT_ABERTO:
	move	$s0, $v0

# #### CRIAR ARQ SAIDA ####
	li	$v0, 13
	la	$a0, nome_arq_saida
	li	$a1, 1
	move	$a2, $zero
	syscall
	bne	$v0, -1 ARQ_SAIDA_CRIADO
	li	$v0, 4
	la	$a0, erro_arq_criar
	syscall
	j	FIM
ARQ_SAIDA_CRIADO:
	move	$s1, $v0


# #### Leitura de Arquivo ####
	move	$a0, $s0
	li	$a2, 1
	move	$t8, $zero
LOOP_LEITURA_ARQ:
	move	$t9, $zero		# Contador de algarismos
	li	$v0, 14			# Ler arquivo
	la	$a1, buffer($t9)
	syscall
	
	beqz	$v0, FIM_LOOP_LEITURA_ARQ
	bne	$v0, $s6, ERRO_LEITURA	# $s6 = 1
	lb	$t0, buffer($t9)
	beq	$t0, '\n', LOOP_LEITURA_ARQ
	
	move	$t2, $zero		# Contador fracionaria
	bne	$t0, '-', SINAL_POS
	li	$t3, -1			# Sinal negativo
	j	LOOP_LEITURA_INT_REAL
SINAL_POS:
	li	$t3, 1			# Sinal negativo
	addi	$t9, $t9, 1		# Como simbolo '+' nao parece no inicio, ja conta um algarismo
	
LOOP_LEITURA_INT_REAL:
	li	$v0, 14			# Ler arquivo
	la	$a1, buffer($t9)
	syscall
	bne	$v0, $s6, ERRO_LEITURA	# $s6 = 1
	lb	$t0, buffer($t9)
	beq	$t0, '.', LOOP_LEITURA_FRAC_REAL
	blt	$t0, '0', FIM_LOOP_LEITURA_REAL		# Comparacao com "-", "+", "\n"
	addi	$t9, $t9, 1
	j	LOOP_LEITURA_INT_REAL
LOOP_LEITURA_FRAC_REAL:
	li	$v0, 14
	la	$a1, buffer($t9)
	syscall
	bne	$v0, $s6, ERRO_LEITURA	# $s6 = 1
	lb	$t0, buffer($t9)
	blt	$t0, '0', FIM_LOOP_LEITURA_REAL		# Comparacao com "-", "+", "\n"
	bgt	$t0, '9', FIM_LOOP_LEITURA_REAL		# Comparacao com "i" 
	addi	$t2, $t2, 1			# Contador de fracionaria++
	addi	$t9, $t9, 1			# Contador de algarismo++
	j	LOOP_LEITURA_FRAC_REAL
FIM_LOOP_LEITURA_REAL:
	move	$t4, $zero			# Valor em inteiro
	li	$t5, 1				# 10^n
LOOP_CONVENCAO_REAL:
	addi	$t9, $t9, -1			# Contador de algarismo--
	blt	$t9, $zero, FIM_LOOP_CONVENCAO_REAL
	lb	$t1, buffer($t9)
	subiu	$t1, $t1, 0x30			# Convencao de 'x' -> x
	mulu	$t1, $t1, $t5			# x * 10^n
	addu	$t4, $t4, $t1			# x * 10^n + x * 10^(n-1)
	mulu	$t5, $t5,  $s7	# $s7 = 10	# 10^(n++)
	j	LOOP_CONVENCAO_REAL
FIM_LOOP_CONVENCAO_REAL:
	mulu	$t4, $t4, $t3			# Define o sinal
	li	$t5, 1				# 10^n
LOOP0:
	beqz	$t2, FIM_LOOP0
	mulu	$t5, $t5,  $s7	# $s7 = 10
	addi	$t2, $t2, -1
	j	LOOP0
FIM_LOOP0:
	mtc1	$t4, $f0
	cvt.s.w	$f0, $f0
	mtc1	$t5, $f1
	cvt.s.w	$f1, $f1
	div.s	$f0, $f0, $f1
	swc1 	$f0, real($t8)
	beq	$t0, '\n', FIM_LEITURA_LINHA_SEM_I
	move	$t9, $zero
	beq	$t0, '-', SINAL_NEG
	li	$t3, 1
	j	LOOP_LEITURA_INT_IMAG
SINAL_NEG:
	li	$t3, -1
	
LOOP_LEITURA_INT_IMAG:
	li	$v0, 14
	la	$a1, buffer($t9)
	syscall
	bne	$v0, $s6, ERRO_LEITURA	# $s6 = 1
	lb	$t0, buffer($t9)
	beq	$t0, '.', LOOP_LEITURA_FRAC_IMAG
	blt	$t0, '0', FIM_LOOP_LEITURA_IMAG
	bgt	$t0, '9', FIM_LOOP_LEITURA_IMAG
	addi	$t9, $t9, 1
	j	LOOP_LEITURA_INT_IMAG
LOOP_LEITURA_FRAC_IMAG:
	li	$v0, 14
	la	$a1, buffer($t9)
	syscall
	bne	$v0, $s6, ERRO_LEITURA	# $s6 = 1
	lb	$t0, buffer($t9)
	blt	$t0, '0', FIM_LOOP_LEITURA_IMAG
	bgt	$t0, '9', FIM_LOOP_LEITURA_IMAG
	addi	$t2, $t2, 1
	addi	$t9, $t9, 1
	j	LOOP_LEITURA_FRAC_IMAG
FIM_LOOP_LEITURA_IMAG:
	#move	$t3, $t0
	move	$t4, $zero
	li	$t5, 1
LOOP_CONVENCAO_IMAG:
	addi	$t9, $t9, -1
	blt	$t9, $zero, FIM_LOOP_CONVENCAO_IMAG
	lb	$t1, buffer($t9)
	subiu	$t1, $t1, 0x30
	mulu	$t1, $t1, $t5
	addu	$t4, $t4, $t1
	mulu	$t5, $t5,  $s7	#$s7 = 10
	j	LOOP_CONVENCAO_IMAG
FIM_LOOP_CONVENCAO_IMAG:
	mulu	$t4, $t4, $t3
	li	$t5, 1
LOOP1:
	beqz	$t2, FIM_LOOP1
	mulu	$t5, $t5,  $s7	#$s7 = 10
	addi	$t2, $t2, -1
	j	LOOP1
FIM_LOOP1:
	mtc1	$t4, $f0
	cvt.s.w	$f0, $f0
	mtc1	$t5, $f1
	cvt.s.w	$f1, $f1
	div.s	$f0, $f0, $f1
	swc1 	$f0, imag($t8)
	li	$v0, 14
	la	$a1, buffer	#Pula um '\n'
	syscall
	
FIM_LEITURA_LINHA_SEM_I:
	addi	$t8, $t8, 4
	j	LOOP_LEITURA_ARQ
FIM_LOOP_LEITURA_ARQ:
	

# #### FFT ####	
	move	$t4, $zero	# n
LOOP_FFT0:
	move	$t5, $zero	# k
LOOP_MENOR:
	sll	$t1, $t5, 2	# 4 * k
	mulu	$t0, $t1, $t4	#  4 * k * n
	
	mtc1	$t0, $f0	# 2PI/N *4*k*n 
	cvt.d.w	$f0, $f0
	mul.d	$f6, $f0, $f28
	
	jal	func_trigonom
	
	sll	$t0, $t5, 2	# (4 * k )+ 0
	
	sll	$t1, $t0, 2	# 4 * k	float
	sll	$t2, $t4, 3	# n	double

	lwc1	$f4, real($t1)	# real[4 * k]
	lwc1	$f6, imag($t1)	# imag[4 * k]
	cvt.d.s	$f4, $f4
	cvt.d.s	$f6, $f6
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	ldc1	$f2, A_real($t2)	# A_real[n]
	add.d	$f2, $f2, $f0
	sdc1	$f2, A_real($t2)
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	ldc1	$f2, A_imag($t2)	# A_imag[n]
	add.d	$f2, $f2, $f0
	sdc1	$f2, A_imag($t2)
	
	addi	$t1, $t0, 1	# (4 * k) + 1
	sll	$t1, $t1, 2	# (4 * k) + 1 float
	
	lwc1	$f4, real($t1)	# real[(4 * k)+1]
	lwc1	$f6, imag($t1)	# imag[(4 * k)+1]
	cvt.d.s	$f4, $f4
	cvt.d.s	$f6, $f6
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	ldc1	$f2, B_real($t2)	# B_real[n]
	add.d	$f2, $f2, $f0
	sdc1	$f2, B_real($t2)
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	ldc1	$f2, B_imag($t2)	# B_imag[n]
	add.d	$f2, $f2, $f0
	sdc1	$f2, B_imag($t2)
	
	addi	$t1, $t0, 2	# (4 * k) + 2
	sll	$t1, $t1, 2	# (4 * k) + 2	float
		
	add	$t2, $t4, $s3	# n + N/4
	sll	$t2, $t2, 3
	
	lwc1	$f4, real($t1)	# real[(4 * k) + 2]
	lwc1	$f6, imag($t1)	# imag[(4 * k) + 2]
	cvt.d.s	$f4, $f4
	cvt.d.s	$f6, $f6
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	ldc1	$f2, A_real($t2)	# A_real[n+N/4]
	add.d	$f2, $f2, $f0
	sdc1	$f2, A_real($t2)
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	ldc1	$f2, A_imag($t2)	# A_imag[n+N/4]
	add.d	$f2, $f2, $f0
	sdc1	$f2, A_imag($t2)
	
	
	addi	$t1, $t0, 3	# (4 * k) + 3
	sll	$t1, $t1, 2	# (4 * k) + 3	float
	
	lwc1	$f4, real($t1)	# real[(4 * k) + 3]
	lwc1	$f6, imag($t1)	# imag[(4 * k) + 3]
	cvt.d.s	$f4, $f4
	cvt.d.s	$f6, $f6
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	ldc1	$f2, B_real($t2)	# B_real[n+N/4]
	add.d	$f2, $f2, $f0
	sdc1	$f2, B_real($t2)
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	ldc1	$f2, B_imag($t2)	# B_imag[n+N/4]
	add.d	$f2, $f2, $f0
	sdc1	$f2, B_imag($t2)	

	addi	$t5, $t5, 1	# k++
	blt	$t5, $s3, LOOP_MENOR
#FIM LOOP MENOR

	sll	$t0, $t4, 1	# 2 * n
	
	mtc1	$t0, $f0	# 2PI / N *2*n
	cvt.d.w	$f0, $f0
	mul.d	$f6, $f0, $f28
	
	jal	func_trigonom
					# BaW = Ba * Wn/2
	ldc1	$f4, A_real($t2)	# A_real[n+N/4]	
	ldc1	$f6, A_imag($t2)	# A_imag[n+N/4]
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	sdc1	$f0, A_real($t2)	# A_real[n+N/4]	
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	sdc1	$f0, A_imag($t2)	# A_imag[n+N/4]
	
					# BbW = Bb * Wn/2
	ldc1	$f4, B_real($t2)	# B_real[n+N/4]
	ldc1	$f6, B_imag($t2)	# B_imag[n+N/4]
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	sdc1	$f0, B_real($t2)	# B_real[n+N/4]
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	sdc1	$f0, B_imag($t2)	# B_imag[n+N/4]
	
	addi	$t4, $t4, 1	# n++	
	blt	$t4, $s3, LOOP_FFT0
#FIM LOOP FFT0

	move	$t4, $zero	# n = 0
LOOP_FFT1:
	add	$t5, $t4, $s3	# n + N/4
	
	sll	$t0, $t4, 3	# n	double
	sll	$t1, $t5, 3	# n+N/4 double

	ldc1	$f4, A_real($t0)	# A_real[n]
	ldc1	$f6, A_real($t1)	# A_real[n+N/4]
	add.d	$f0, $f4, $f6		# An     = Aa + BaW
	sub.d	$f2, $f4, $f6		# An+N/4 = Aa - BaW
	sdc1	$f0, A_real($t0)
	sdc1	$f2, A_real($t1)
	
	ldc1	$f4, A_imag($t0)	# A_imag[n]
	ldc1	$f6, A_imag($t1)	# A_imag[n+N/4]
	add.d	$f0, $f4, $f6		# An     = Aa + BaW
	sub.d	$f2, $f4, $f6		# An+N/4 = Aa - BaW
	sdc1	$f0, A_imag($t0)
	sdc1	$f2, A_imag($t1)

	
	ldc1	$f4, B_real($t0)	# B_real[n]
	ldc1	$f6, B_real($t1)	# B_real[n+N/4]
	add.d	$f0, $f4, $f6		# Bn     = Ab + BbW
	sub.d	$f2, $f4, $f6		# Bn+N/4 = Ab - BbW
	sdc1	$f0, B_real($t0)
	sdc1	$f2, B_real($t1)
	
	ldc1	$f4, B_imag($t0)	# B_imag[n]
	ldc1	$f6, B_imag($t1)	# B_imag[n+N/4]
	add.d	$f0, $f4, $f6		# Bn     = Ab + BbW
	sub.d	$f2, $f4, $f6		# Bn+N/4 = Ab - BbW
	sdc1	$f0, B_imag($t0)
	sdc1	$f2, B_imag($t1)
	
	
	mtc1	$t4, $f0	# n
	cvt.d.w	$f0, $f0
	mul.d	$f6, $f0, $f28	# 2PI / N *n

	jal	func_trigonom

	move	$t0, $t4	# n
	add	$t1, $t4, $s4	# n + N/2
	sll	$t6, $t0, 2	# n	float
	sll	$t7, $t1, 2	# n+N/2	float
	sll	$t0, $t0, 3	# n	double
	
					# BW = B*Wn
	ldc1	$f4, B_real($t0)	# B_real[n]
	ldc1	$f6, B_imag($t0)	# B_imag[n]
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	sdc1	$f0, B_real($t0)
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	sdc1	$f0, B_imag($t0)
		
	ldc1	$f4, A_real($t0)	# A_real[n]
	ldc1	$f6, B_real($t0)	# B_real[n]
		
	add.d	$f0, $f4, $f6		# Xn     = A + BW
	sub.d	$f2, $f4, $f6		# Xn+N/2 = A + BW
	
	div.d	$f0, $f0, $f20		# Xn     / 10
	div.d	$f2, $f2, $f20		# Xn+N/2 / 10
	
	cvt.s.d	$f0, $f0
	cvt.s.d	$f2, $f2
	swc1	$f0, real($t6)		# real[n]
	swc1	$f2, real($t7)		# real[n+N/2]
	
	ldc1	$f4, A_imag($t0)
	ldc1	$f6, B_imag($t0)	
	add.d	$f0, $f4, $f6		# Xn     = A + BW
	sub.d	$f2, $f4, $f6		# Xn+N/2 = A + BW
	div.d	$f0, $f0, $f20		# Xn     / 10
	div.d	$f2, $f2, $f20		# Xn+N/2 / 10
	cvt.s.d	$f0, $f0
	cvt.s.d	$f2, $f2
	swc1	$f0, imag($t6)
	swc1	$f2, imag($t7)
	
	
	add	$t5, $t4, $s3	# n + N/4
	mtc1	$t5, $f0
	cvt.d.w	$f0, $f0
	mul.d	$f6, $f0, $f28	# 2PI*(n+N/4) / N 

	jal	func_trigonom

	
	move	$t0, $t5	# n + N/4
	add	$t1, $t5, $s4	# n + N/4 + N/2
	sll	$t6, $t0, 2	# n+N/4	float
	sll	$t7, $t1, 2	# n+N/4+N/2	float
	sll	$t0, $t0, 3	# n+N/4	double
	
					# BW = B*W
	ldc1	$f4, B_real($t0)	# B_real[n+N/4]
	ldc1	$f6, B_imag($t0)	# B_imag[n+N/4]
	
	mul.d	$f0, $f4, $f8		# real*cos - imag *sen
	mul.d	$f2, $f6, $f10
	sub.d	$f0, $f0, $f2
	sdc1	$f0, B_real($t0)
	
	mul.d	$f0, $f4, $f10		# real*sen + imag *cos
	mul.d	$f2, $f6, $f8
	add.d	$f0, $f0, $f2
	sdc1	$f0, B_imag($t0)

	ldc1	$f4, A_real($t0)	# A_real[n+N/4]
	ldc1	$f6, B_real($t0)	# B_real[n+N/4]
	add.d	$f0, $f4, $f6		# Xn+N/4     = A + BW
	sub.d	$f2, $f4, $f6		# Xn+N/4+N/2 = A + BW
	div.d	$f0, $f0, $f20		# Xn+N/4     / 10
	div.d	$f2, $f2, $f20		# Xn+N/4+N/2/ 10
	cvt.s.d	$f0, $f0
	cvt.s.d	$f2, $f2
	swc1	$f0, real($t6)		# real[n+N/4]
	swc1	$f2, real($t7)		# real[n+N/4+N/2]
	
	ldc1	$f4, A_imag($t0)
	ldc1	$f6, B_imag($t0)	
	add.d	$f0, $f4, $f6		# Xn+N/4     = A + BW
	sub.d	$f2, $f4, $f6		# Xn+N/4+N/2 = A + BW
	div.d	$f0, $f0, $f20		# Xn+N/4     / 10
	div.d	$f2, $f2, $f20		# Xn+N/4+N/2 / 10
	cvt.s.d	$f0, $f0
	cvt.s.d	$f2, $f2
	swc1	$f0, imag($t6)
	swc1	$f2, imag($t7)

	addi	$t4, $t4, 1	# n++
	blt	$t4, $s3, LOOP_FFT1
#FIM LOOP_FFT1

# #### GRAVAR NO ARQUIVO ####
	move	$t3, $zero
	sll	$t4, $s5, 2
LOOP_GRAVA:
	lwc1	$f0, real($t3)	
	jal	grava_valor
	addi	$t3, $t3, 4
	blt	$t3, $t4, LOOP_GRAVA
FIM:
	move	$a0, $s0
	li	$v0, 16
	syscall
	move	$a0, $s1
	li	$v0, 16
	syscall
	li	$v0, 10			#termina o programa
	syscall

ERRO_LEITURA:
	j	FIM
	
grava_valor:
	move	$t7, $zero
	move	$t8, $zero
	move	$t9, $zero
	li	$t0, 0x461c4000		# 1000.0 float
	mtc1	$t0, $f1
	mul.s	$f2, $f0, $f1
	round.w.s $f2,$f2
	#cvt.w.s	$f0, $f0	
	mfc1	$t1, $f2
	li	$t7, 4
	move	$t8, $zero
	li	$t9, 7
	li	$t0, '\n'
	sb	$t0, buffer($t9)
	sb	$t0, buffer+1($t9)
LOOP_FRAC1:
	beq	$t8, $t7, FIM_LOOP_FRAC1
	rem	$t0, $t1, 10
	beqz	$t0, PULA_ZERO
	addiu	$t9, $t9, -1
	addiu 	$t0, $t0, 0x00000030
	sb	$t0, buffer($t9)
PULA_ZERO:
	divu	$t1, $t1, $s7
	addiu	$t8, $t8, 1
	j	LOOP_FRAC1
FIM_LOOP_FRAC1:
	li	$t0, '.'
	addiu	$t9, $t9, -1
	sb	$t0, buffer($t9)
LOOP_INT1:
	beqz	$t1, FIM_LOOP_INT1
	rem	$t0, $t1, $s7
	addiu 	$t0, $t0, 0x00000030
	addiu	$t9, $t9, -1
	sb	$t0, buffer($t9)
	divu	$t1, $t1, $s7
	j	LOOP_INT1
FIM_LOOP_INT1:
	nop
	li	$t8, 7
	sub	$t8, $t8, $t9
	addi	$t8, $t8, 1
	li	$v0, 15			#Grava no arquivo.
	move	$a0, $s1
	la	$a1, buffer($t9)
	move	$a2, $t8
	syscall
	jr	$ra


func_trigonom:
LOOP_AJUSTE_360:
	c.lt.d	$f26, $f6
	bc1f	FIM_LOOP_AJUSTE_360
	sub.d	$f6, $f6, $f26
	j	LOOP_AJUSTE_360
FIM_LOOP_AJUSTE_360:

	c.lt.d	1, $f24, $f6
	bc1f	1, FIM_AJUSTE_180
	sub.d	$f6, $f6, $f24
FIM_AJUSTE_180:
	
	c.lt.d	2, $f22, $f6
	bc1f	2, FIM_AJUSTE_90
	sub.d	$f6, $f6, $f22
FIM_AJUSTE_90:
	
					
	li	$t0, 0x544486e0	# 45 grau double
	li	$t1, 0x3fe921fb
	mtc1.d	$t0, $f4
	li	$t0 0x3ff00000	# 1
	mtc1	$t0, $f9	# ($f8)  cos
	mtc1	$t0, $f11	# ($f10) sen
		
	li	$t9, 1
LOOP_TRIGONOM:
	beq	$t9, 18, FIM_LOOP_TRIGONOM
	c.lt.d	$f4, $f6
	li	$t3, 1
	bc1t 	L0
	li	$t3, -1
L0:
	mtc1.d	$t3, $f2
	cvt.d.w	$f2, $f2
	sll	$t8, $t9, 3
	ldc1	$f0, thetai($t8)
	mul.d,	$f0, $f0, $f2
	addi	$t7, $t9, -1
	li	$t0, 2
	add.d	$f4, $f4, $f0
	
	sllv	$t0, $t0, $t7	
	mtc1.d	$t0, $f0
	cvt.d.w	$f0, $f0
	
	mul.d	$f12, $f10, $f2		# x
	div.d	$f12, $f12, $f0
	sub.d	$f12, $f8, $f12
	
	mul.d	$f14, $f8, $f2		# y
	div.d	$f14, $f14, $f0
	add.d	$f14, $f10, $f14
	
	mov.d	$f8, $f12
	mov.d	$f10, $f14
	
	addi	$t9, $t9, 1
	j	LOOP_TRIGONOM
FIM_LOOP_TRIGONOM:
	div.d	$f8, $f8, $f30		#cos
	div.d	$f10, $f10, $f30	#sen
	
	mtc1	$zero, $f0
	cvt.d.s	$f0, $f0
	bc1f 	2, FIM_AJUSTE_SAIDA_90
	mov.d	$f12, $f8
	sub.d	$f8,  $f0, $f10
	mov.d	$f10, $f12
FIM_AJUSTE_SAIDA_90:
	bc1f 	1, FIM_AJUSTE_SAIDA_180
	sub.d	$f8,  $f0, $f8
	sub.d	$f10,  $f0, $f10
FIM_AJUSTE_SAIDA_180:
	abs.d	$f12, $f8
	c.lt.d	$f12, $f18
	bc1f 	FIM_AJUSTE_ZERO_COS
	mtc1	$zero, $f8
	mtc1	$zero, $f9
FIM_AJUSTE_ZERO_COS:
	abs.d	$f12, $f10
	c.lt.d	$f12, $f18
	bc1f 	FIM_AJUSTE_ZERO_SEN
	mtc1	$zero, $f10
	mtc1	$zero, $f11
FIM_AJUSTE_ZERO_SEN:
	jr	$ra
	
	
ler_string:
	move	$t0, $a0		#Mensagem para imprimir
	move	$t1, $a1		#Endereco do string
	move	$t2, $a1		#Endereco do string
LOOP_STRING:
	move	$t3, $zero		#Reg. temporario para gurdar um caractere
	move	$t9, $zero		#Contador de caracteres
	li	$v0, 4			#Imprime a mensagem
	syscall
	li	$v0, 8			#Le a entrada digitada pelo usuario e guarda no buffer
	la	$a0, buffer
	li	$a1, 50
	syscall
LOOP_BUFFER:
	lb	$t3, buffer($t9)	#Pega um caractere do buffer
	beq	$t3, 0x0a, FIM_BUFFER	#Verifica o caractere carregado. Se for um "\n", chegou no final buffer
	sb	$t3, 0($t2)		#Grava o caractere no string
	addi	$t2, $t2, 1		#Incrementa posicao do string
	addi	$t9, $t9, 1		#Incrementa contador de caractere
	bgtu	$t9, 30, ERRO_N_MAX	#Se o contador supera o limite, ocorre um erro
	j	LOOP_BUFFER
ERRO_N_MAX:
	li	$v0, 4			#Imprime a mensagem de erro e volta no inicio da funcao.
	la	$a0, erro_n_string
	syscall
	move	$a0, $t0		
	move	$t2, $t1
	j	LOOP_STRING
ERRO_ENT_VAZIO:
	li	$v0, 4			#Imprime a menagem de erro e volta no inicio da funcao.
	la	$a0, erro_str_vazio
	syscall
	move	$a0, $t0
	move	$t2, $t1
	j	LOOP_STRING
FIM_BUFFER:
	beqz	$t9, ERRO_ENT_VAZIO	#Se o contador for igual a zero, indica que a entrada foi vazio, ocorrendo um erro.
	sb	$zero, 0($t2)		#Coloca indice de terminacao no string
	jr	$ra
